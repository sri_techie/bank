<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model\Transaction;
use App\Model\Customer;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    return [
        'customer_id' => function(){
    		return Customer::all()->random();	
    	},
        'amount' => $faker->randomFloat($nbMaxDecimals = NULL, 5, 5000), // 48.8932
    ];
});
