<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Model\Transaction;
use App\Model\Customer;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;

class TransactionsControllerTest extends TestCase
{
    private $ouath_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImVhOWY5NWY5ZWUwYmQzYzU5ZmIzNTQyMDE1NGIyMDk0MmE3MGU3ZmE4NWEwMTJmYzljODhlN2ViY2ExNjhiNjA0MjJlMjEyNTgzNTExOTY4In0.eyJhdWQiOiIyIiwianRpIjoiZWE5Zjk1ZjllZTBiZDNjNTlmYjM1NDIwMTU0YjIwOTQyYTcwZTdmYTg1YTAxMmZjOWM4OGU3ZWJjYTE2OGI2MDQyMmUyMTI1ODM1MTE5NjgiLCJpYXQiOjE1NjE3MjQxNzUsIm5iZiI6MTU2MTcyNDE3NSwiZXhwIjoxNTkzMzQ2NTc1LCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.M9JH0dcWn-K15I4gIJCdO4EaGzRFcYiiStdyBkfhLvrjtHbCL8wYeTSatPuAzYzOUDQg2etexrMgDGG1HjKsi6BNeUVQrQhkp-vAVmnSDDSJUGxug2xOH4mRFkSz4xWsAHmMzxvs4PXh-JgxLOfgu0MioY3jtwW6bfvspOKO7HeWJCG10-P23oKYopiLv3ruiMIPm2KAnmJojk9Ek7BnIfWLQ8rgkoD2n4EeeIS1Rv5krhLT_lUgE6hn8BKCULW0UUYnk0NdKFPU53JZzpl2PoASYj_7wWbOOVFwfIUpkOFcl3HhLyHs2UFs_c4rfNDN24o24-b8fd83Oy0cE3D4tA9qpUdvH2w3fMzC4aSum_TOLQAfYRXjp6qFbwgy_GCayFWdncRKEW1TpS3w-p-MAucyfyKG8rdOFrqttcIRphyOpgZbXcJygo0Ki5TWO571WUbLNmdqOdogyvgRtRWYiiFYyEfe55wFNc9_oEGKTFllkwYrnrk8n7_hld5E4sIakM129x--kqJKN-eKxKnL098pfsUmRUmi0_5clwfW9F4M9n_5TILd5VLq0uB_SvAeoZgd6JPSEu-4Q8-ppzS_qFAwifaCc5zsgtDFcoC1hqgpOpFAcminylo6Lo85TxVPLrmlj9nc0Y0d7kTjiTpOzs4uZisnlkAs7exsrWV3qp4";

    /**
	 * Test that transaction can be created
	 *
	 * @return void
	 */
	public function testTransactionCreate() {
        $customer = factory(Customer::class)->create(['name' => 'Dave', 'cnp' => 'No']);
        $transaction = factory(Transaction::class)->make(['customer_id' => $customer->id, 'amount' => 200]);
		
		$response = $this
			->withHeader('Authorization', 'Bearer ' . $this->ouath_token)
            ->json('POST', route('transactions.store', $customer->id), $transaction->toArray());
  
        // Check status
        $response->assertStatus(Response::HTTP_CREATED);
        
		// Check that the return json structure cotnains the new transaction
		$res_array = (array)json_decode($response->content())->data;
		
		// check the transaction is in the database
		$this->assertDatabaseHas('transactions', ["amount" => 200]);
        $this->assertDatabaseHas('transactions', ["customer_id" => $customer->id]);
        
    }
    
    /**
	 * Test that transaction can be created
	 *
	 * @return void
	 */
	public function testTransactionShow() {
        $newTransaction = self::createTransaction(850);
        $res = json_decode($newTransaction);
        
        
        if ($res->data->transactionId) {
			$responseShow = $this
				->withHeader('Authorization', 'Bearer ' . $this->ouath_token)
                ->json('GET',  route('transactions.show', ['customer' => $res->data->customerId, 'transaction' => $res->data->transactionId]));

            $responseShow->assertStatus(Response::HTTP_OK);
            
            $res_array = (array) json_decode($responseShow->getContent());
            
            $this->assertCount(1, $res_array);
            
			// test couple of the parameters
            $this->assertEquals($res_array['data']->transactionId, $res->data->transactionId);
            $this->assertEquals($res_array['data']->amount, 850);
            
		}
        
    }

    public function createTransaction( $amount = 400) {
        $customer = factory(Customer::class)->create(['name' => 'Dave', 'cnp' => 'No']);
        $transaction = factory(Transaction::class)->make(['customer_id' => $customer->id, 'amount' => $amount]);

		$response = $this
			->withHeader('Authorization', 'Bearer ' . $this->ouath_token)
            ->json('POST', route('transactions.store', $customer->id), $transaction->toArray());
            
		return $response->getContent();
	}
    
}
