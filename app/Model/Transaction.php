<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
		'amount'
    ];
    
    public function customer()
    {
    	return $this->belongsTo(Customer::class); 
    }
}
