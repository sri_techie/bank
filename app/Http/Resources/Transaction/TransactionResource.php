<?php

namespace App\Http\Resources\Transaction;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'transactionId' => $this->id,
            'customerId' => $this->customer_id,
            'amount' => $this->amount,
            'date' => $this->created_at,
        ];
    }
}
