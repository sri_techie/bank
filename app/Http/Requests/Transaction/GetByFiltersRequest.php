<?php

namespace App\Http\Requests\Transaction;

use Illuminate\Foundation\Http\FormRequest;

class GetByFiltersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|integer|between:20,5000',//some random integer for simplicity
            'date' => 'required|date',
            'offset' => 'required|integer|between:0,50',//some random offset
            'limit' => 'required|integer|between:1,50',//some random limit
        ];
    }
}
