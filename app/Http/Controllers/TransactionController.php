<?php

namespace App\Http\Controllers;

use App\Model\Transaction;
use App\Model\Customer;
use Illuminate\Http\Request;
use App\Http\Requests\Transaction\GetByFiltersRequest;
use App\Http\Requests\Transaction\StoreRequest;
use App\Http\Requests\Transaction\UpdateRequest;
use App\Http\Requests\Transaction\AccessRequest;
use App\Http\Resources\Transaction\TransactionCollection;
use App\Http\Resources\Transaction\TransactionResource;
use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Customer $customer)
    {
        return TransactionResource::collection($customer->transactions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, Customer $customer)
    {
        $transaction = new Transaction($request->all());
        $customer->transactions()->save($transaction);
        return response([
            'data' => new TransactionResource($transaction)
        ],Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(AccessRequest $request, Customer $customer, Transaction $transaction)
    {
        return new TransactionCollection($transaction);
    }

    /**
     * Display the specified resource by filters.
     *
     * @param  \App\Model\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function getByFilters(GetByFiltersRequest $request, Customer $customer)
    {
        // return $customer;
        // return $request->date;
        $query = $request->customer->transactions();
        $query->where('amount', $request->amount);
        $query->whereDate('created_at', '=', Carbon::create($request->date)->toDateString());
        $query->offset($request->offset);
        $query->limit($request->limit);
        
        $transactions = $query->get();

        return TransactionResource::collection($transactions);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Transaction $transaction)
    {
        $transaction->update($request->all());
        return response([
            'data' => new TransactionResource($transaction)
        ],Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccessRequest $request, Transaction $transaction)
    {
        try {
			$transaction->delete();
            return response()->json(['data' => ['success.']]);
        
		} catch (\Exception $e) {
			Log::error($e);
            return response()->json(['data' => ['fail']])->setStatusCode(500);
		}
    }
}
